import { default as Table } from "antd/es/table";
import "antd/es/table/style/index.css";
import { default as Modal } from "antd/es/modal";
import "antd/es/modal/style/css";
import { default as Button } from "antd/es/button";
import "antd/es/button/style/css";
import { default as Form } from "antd/es/form";
import "antd/es/form/style/css";
import { default as Input } from "antd/es/input";
import "antd/es/input/style/css";
import { default as DatePicker } from "antd/es/date-picker";
import "antd/es/date-picker/style/css";
import "antd/es/date-picker/style/css";
import { default as Space } from "antd/es/space";
import "antd/es/space/style/css";
import { default as Select } from "antd/es/select";
import "antd/es/select/style/css";
import { useEffect, useState } from "react";
import api from "../../constants/api";

interface CardData {
  id?: string;
  name: string;
  link: string;
  status: string;
  prizePool: string;
  timeStart: number;
  timeEnd: number;
  totalPaid: string;
  currency: {
    code: string;
  };
}

interface CreateEvent {
  name: string;
  link: string;
  timeStart: number;
  timeEnd: number;
  prizePool: number;
  currency: string;
}

const columns = [
  {
    title: "name",
    dataIndex: "name",
    key: "name",
  },
  {
    title: "link",
    dataIndex: "link",
    key: "link",
  },
  {
    title: "time",
    dataIndex: "time",
    key: "time",
  },
  {
    title: "status",
    dataIndex: "status",
    key: "status",
  },
  {
    title: "prizePool",
    dataIndex: "prizePool",
    key: "prizePool",
  },
  {
    title: "totalPaid",
    dataIndex: "totalPaid",
    key: "totalPaid",
  },
];

export default function BonusEvent() {
  const [data, setData] = useState([]);
  const [isModalVisible, setIsModalVisible] = useState(false);
  const { RangePicker } = DatePicker;
  const { Option } = Select;

  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleChange = (value: string) => {
    console.log(`selected ${value}`);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  useEffect(() => {
    getData();
  }, []);

  const getData = async () => {
    const res = await api.get("/cms/bonus/bonus-events?size=100");
    setData(
      res.data.data.map((row: CardData) => ({
        key: row.id,
        name: row.name,
        link: row.link,
        status: row.status,
        prizePool: `${row.prizePool}  ${row.currency.code}`,
        time: `${new Date(row.timeStart)
          .toISOString()
          .split("T")[0]
          .replaceAll("-", "/")} - ${new Date(row.timeEnd)
          .toISOString()
          .split("T")[0]
          .replaceAll("-", "/")}`,
        totalPaid: `${row.totalPaid}  ${row.currency.code}`,
      }))
    );
  };

  const onFinish = (values: CreateEvent) => {
    api.post("/cms/bonus/new-bonus-event", values).then((res: any) => {
      localStorage.setItem("token", res.data.token);
      window.location.href = "/list";
    });
  };

  return (
    <>
      <Button type="primary" onClick={showModal}>
        Add
      </Button>
      <Modal
        title="Add event"
        visible={isModalVisible}
        onCancel={handleCancel}
        footer={""}
      >
        <Form
          labelCol={{
            span: 8,
          }}
          wrapperCol={{
            span: 16,
          }}
          onFinish={onFinish}
        >
          <Form.Item label="name" name="name">
            <Input />
          </Form.Item>
          <Form.Item label="link" name="link">
            <Input />
          </Form.Item>
          <Form.Item label="time" name="time">
            <Space direction="vertical" size={12}>
              <RangePicker renderExtraFooter={() => "extra footer"} />
            </Space>
          </Form.Item>
          <Form.Item label="token" name="token">
            <Select
              defaultValue="BBC"
              style={{ width: 120 }}
              onChange={handleChange}
            >
              <Option value="BBC">BBC</Option>
              <Option value="SAT">SAT</Option>
            </Select>
          </Form.Item>
          <Form.Item label="prizePool" name="prizePool">
            <Input />
          </Form.Item>
          <Form.Item>
            <Space>
              <Button key="back" onClick={handleCancel}>
                Cancel
              </Button>
              <Button key="import" htmlType="submit" type="primary">
                Submit
              </Button>
            </Space>
          </Form.Item>
        </Form>
      </Modal>
      <div>
        <Table dataSource={data} columns={columns} />
      </div>
    </>
  );
}
