import { default as Button } from "antd/es/button";
import "antd/es/table/style/index.css";
import { default as Form } from "antd/es/form";
import "antd/es/form/style/index.css";
import { default as Input } from "antd/es/input";
import "antd/es/input/style/index.css";
import api from "../../constants/api";
import "./index.css";

interface User {
  userName: string;
  password: string;
}

interface LoginType {
  data: { username: string; expireIn: number; token: string };
}

export default function LoginPage() {
  const onFinish = (values: User) => {
    api.post("/cms/auth/sign-in", values).then((res: LoginType) => {
      localStorage.setItem("token", res.data.token);
      window.location.href = "/list";
    });
  };

  const onFinishFailed = (errorInfo: any) => {
    console.log("Failed:", errorInfo);
  };

  return (
    <div className="swapper-login">
      <Form
        name="basic"
        labelCol={{
          span: 8,
        }}
        wrapperCol={{
          span: 16,
        }}
        initialValues={{
          remember: true,
        }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
      >
        <Form.Item
          label="Username"
          name="username"
          rules={[
            {
              required: true,
              message: "Please input your username!",
            },
          ]}
        >
          <Input style={{ width: "500px" }} />
        </Form.Item>

        <Form.Item
          label="Password"
          name="password"
          rules={[
            {
              required: true,
              message: "Please input your password!",
            },
          ]}
        >
          <Input.Password style={{ width: "500px" }} />
        </Form.Item>

        <Form.Item
          wrapperCol={{
            offset: 8,
            span: 16,
          }}
        >
          <Button
            type="primary"
            htmlType="submit"
            style={{ marginLeft: "200px" }}
          >
            Submit
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
}
