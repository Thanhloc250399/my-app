import ReactDOM from "react-dom/client";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import LoginPage from "./pages/login";
import BonusEvent from "./pages/BonusEvent";
import AddEvent from "./pages/BonusEvent/addEvent";

const root = ReactDOM.createRoot(
  document.getElementById("root") as HTMLElement
);
root.render(
  <BrowserRouter>
    <Routes>
      <Route path="/" element={<LoginPage />} />
      <Route path="/list" element={<BonusEvent />} />
      <Route path="/create" element={<AddEvent />} />
    </Routes>
  </BrowserRouter>
);
