import axios from "axios";
const API_URL = process.env.API_URL || "http://192.168.10.53:3005";

const token = localStorage.getItem("token");

const api = axios.create({
  baseURL: `${API_URL}`,
});

api.interceptors.request.use(
  (config: any) => {
    if (token) {
      config.headers["Authorization"] = `Bearer ${token}`;
    }
    return config;
  },
  (error) => Promise.reject(error)
);

export default api;
